﻿using System;
using System.ComponentModel;
using System.Reflection;
using System.Windows.Controls;
using System.Windows.Input;

namespace ControlAddToTableS
{
    /// <summary>
    /// Логика взаимодействия для AddToTableControlS.xaml
    /// </summary>
    public partial class AddToTableControlS : UserControl
    {
        public event EventHandler<ModelControl> OnButton;
        public ModelControl model = new ModelControl();

        public AddToTableControlS()
        {
            InitializeComponent();
            Init();
        }

        private void Init()
        {
            model = PropertyMap.SelectedObject as ModelControl;
            model.PropertyChanged += Model_PropertyChanged;
        }

        private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "InterferenceParam")
            {
                if (model.InterferenceParam == InterferenceP.ThirdParam)
                {
                    ChangeBrowsableAttribute(true, "SpSc");
                    ChangeBrowsableAttribute(true, "Dev");
                    ChangeBrowsableAttribute(false, "Man");
                }
                else if (model.InterferenceParam == InterferenceP.SecondParam)
                {
                    ChangeBrowsableAttribute(true, "Man");
                    ChangeBrowsableAttribute(false, "Dev");
                    ChangeBrowsableAttribute(false, "SpSc");
                }
                else if (model.InterferenceParam == InterferenceP.FirstParam)
                {
                    ChangeBrowsableAttribute(false, "Man");
                    ChangeBrowsableAttribute(false, "Dev");
                    ChangeBrowsableAttribute(false, "SpSc");
                }
                Init();
            }
        }

        private void ChangeBrowsableAttribute(bool value, string property)
        {
            PropertyDescriptor pDesc = TypeDescriptor.GetProperties(model.GetType())[property];
            BrowsableAttribute attrib = (BrowsableAttribute)pDesc.Attributes[typeof(BrowsableAttribute)];
            FieldInfo isBrowsable = attrib.GetType().GetField("browsable", BindingFlags.NonPublic | BindingFlags.Instance);
            isBrowsable.SetValue(attrib, value);
            var item = PropertyMap.SelectedObject as ModelControl;
            PropertyMap.SelectedObject = new ModelControl() { FKHz = item.FKHz, Man = item.Man, Dev = item.Dev, DFKHz = item.DFKHz, InterferenceParam = item.InterferenceParam, SpSc = item.SpSc, Power = item.Power };
            Init();
        }

        public void OnAdd()
        {
            ChangeBrowsableAttribute(false, "SpSc");
            ChangeBrowsableAttribute(false, "Dev");
            ChangeBrowsableAttribute(false, "Man");
            model.InterferenceParam = InterferenceP.ThirdParam;
            model.InterferenceParam = InterferenceP.SecondParam;
            model.InterferenceParam = InterferenceP.FirstParam;
        }

        public void OnChange(ModelControl Model)
        {
            if (Model.InterferenceParam == InterferenceP.FirstParam)
            {
                PropertyMap.SelectedObject = new ModelControl() { FKHz = Model.FKHz, Man = Model.Man, Dev = Model.Dev, DFKHz = Model.DFKHz, InterferenceParam = Model.InterferenceParam, SpSc = Model.SpSc, Power = Model.Power };
                ChangeBrowsableAttribute(false, "SpSc");
                ChangeBrowsableAttribute(false, "Dev");
                ChangeBrowsableAttribute(false, "Man");
            }
            else if (Model.InterferenceParam == InterferenceP.SecondParam)
            {
                PropertyMap.SelectedObject = new ModelControl() { FKHz = Model.FKHz, Man = Model.Man, Dev = Model.Dev, DFKHz = Model.DFKHz, InterferenceParam = Model.InterferenceParam, SpSc = Model.SpSc, Power = Model.Power };
                ChangeBrowsableAttribute(true, "Man");
                ChangeBrowsableAttribute(false, "Dev");
                ChangeBrowsableAttribute(false, "SpSc");
            }
            else if (Model.InterferenceParam == InterferenceP.ThirdParam)
            {
                PropertyMap.SelectedObject = new ModelControl() { FKHz = Model.FKHz, Man = Model.Man, Dev = Model.Dev, DFKHz = Model.DFKHz, InterferenceParam = Model.InterferenceParam, SpSc = Model.SpSc, Power = Model.Power };
                ChangeBrowsableAttribute(false, "Man");
                ChangeBrowsableAttribute(true, "Dev");
                ChangeBrowsableAttribute(true, "SpSc");
            }
        }
        
        private void ButApply_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            ModelControl control = new ModelControl();
            Init();
            if (model.InterferenceParam == InterferenceP.FirstParam)
            {
                control = new ModelControl() { FKHz = model.FKHz, DFKHz = model.DFKHz, InterferenceParam = model.InterferenceParam, Dev = 0, Man = 0, Power = model.Power };
            }
            else if (model.InterferenceParam == InterferenceP.SecondParam)
            {
                control = new ModelControl() { FKHz = model.FKHz, DFKHz = model.DFKHz, InterferenceParam = model.InterferenceParam, Dev = 0, Man = model.Man, Power = model.Power };
            }
            else
            {
                control = new ModelControl() { FKHz = model.FKHz, DFKHz = model.DFKHz, InterferenceParam = model.InterferenceParam, Dev = model.Dev, Man = model.SpSc, Power = model.Power };
            }
            OnButton?.Invoke(this, control);
        }
        
    }
}

