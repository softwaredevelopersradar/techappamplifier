﻿using System;
using System.Collections.Generic;
using System.Windows.Media.Imaging;

namespace WPF_SHS
{
    public class LetterEventArgs : EventArgs
    {
        public byte Letter { get; private set; }

        public LetterEventArgs( byte Letter)
        {
            this.Letter = Letter;
        }
    }

    public class SetSpoofEventArgs : EventArgs
    {
        public bool PowerBool { get; private set; }
        public byte PowerByte { get; private set; }

        public SetSpoofEventArgs(bool PowerBool, byte PowerByte)
        {
            this.PowerBool = PowerBool;
            this.PowerByte = PowerByte;
        }
    }

    public class SetNaViEventArgs : EventArgs
    {
        public bool GPS_L1 { get; private set; }
        public bool GPS_L2 { get; private set; }
        public bool Glonass_L1 { get; private set; }
        public bool Glonass_L2 { get; private set; }
        public bool PowerBool { get; private set; }
        public byte PowerByte { get; private set; }

        public SetNaViEventArgs(bool GPS_L1, bool GPS_L2,bool Glonass_L1, bool Glonass_L2, bool PowerBool, byte PowerByte)
        {
            this.GPS_L1 = GPS_L1;
            this.GPS_L2 = GPS_L2;
            this.Glonass_L1 = Glonass_L1;
            this.Glonass_L2 = Glonass_L2;
            this.PowerBool = PowerBool;
            this.PowerByte = PowerByte;
        }
    }

    public class SetPresEventArgs : EventArgs
    {
        public bool[] Pres { get; private set; }

        public SetPresEventArgs(bool[] Pres)
        {
            this.Pres = Pres;
        }
    }

    public class ParamEventArgs : EventArgs
    {
        public List<TableFreq> Tables { get; private set; }
        public bool PowerBool { get; private set; }
        public byte[] PowerByte { get; private set; }

        public ParamEventArgs(List<TableFreq> Tables, bool PowerBool, byte[] PowerByte)
        {
            this.Tables = Tables;
            this.PowerBool = PowerBool;
            this.PowerByte = PowerByte;
        }
        
    }

    public class SetParamFWSGNSSEventArgs : EventArgs
    {
        public SetNaViEventArgs NaVi { get; private set; }
        public ParamEventArgs Param { get; private set; }

        public SetParamFWSGNSSEventArgs(SetNaViEventArgs NaVi, ParamEventArgs Param)
        {
            this.NaVi = NaVi;
            this.Param = Param;
        }
    }

    public class ForSecondTable : EventArgs
    {
        public BitmapImage Signal { get; set; }
        public BitmapImage Emitting { get; set; }
        public BitmapImage Power { get; set; }
        public bool Error { get; set; }

        public ForSecondTable(BitmapImage Signal, BitmapImage Emitting, BitmapImage Power , bool Error)
        {
            this.Signal = Signal;
            this.Emitting = Emitting;
            this.Power = Power;
            this.Error = Error;
        }
    }
}
