﻿using SHS_DLL;
using System;
using System.IO;
using System.IO.Ports;
using System.Windows;
using System.Windows.Media;

namespace WPF_SHS
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            GrozaSButton.Background= Brushes.Gray;
            GrozaZ1.Visibility = Visibility.Collapsed;
            GrozaS.Visibility = Visibility.Visible;
            IntitComPort();

            ReadConfig();

            sHS.OnWriteByte += SHS_OnWriteByte;
            sHS.OnReadByte += SHS_OnReadByte;
            sHS.OnDisconnect += SHS_OnDisconnect;

            GrozaS.OnStatus += GrozaS_OnStatus;
            GrozaS.OnSetNaVi += GrozaS_OnSetNaVi;
            GrozaS.OnParam += GrozaS_OnParam;
            GrozaS.OnSpoof += GrozaS_OnSpoof;
            GrozaS.OnOffRad += GrozaS_OnOffRad;
            GrozaS.OnReset += GrozaS_OnReset;
            GrozaS.OnRelaySwitching += GrozaS_OnRelaySwitching;

            GrozaZ1.OnFullStatus += GrozaZ1_OnFullStatus;
            GrozaZ1.OnOffRadAndSetPres += GrozaZ1_OnOffRadAndSetPres;
            GrozaZ1.OnSetPres += GrozaZ1_OnSetPres;
            GrozaZ1.OnSetParam += GrozaZ1_OnSetParam;

            sHS.OnConfirmStatus += SHS_OnConfirmStatus;
            sHS.OnConfirmSet += SHS_OnConfirmSet;
            sHS.OnConfirmFullStatus += SHS_OnConfirmFullStatus;
            

            Display.DisplayTime = false;
        }
        
        #region Config
        private string COM;
        private string Baudrate;

        private void ReadConfig()
        {
            try
            {
                int count = 0;
                using (StreamReader sr = new StreamReader($"{AppDomain.CurrentDomain.BaseDirectory}Config.txt"))
                {
                    COM = sr.ReadLine();
                    Baudrate = sr.ReadLine();
                }
                foreach(var i in ComBox.Items)
                {
                    if (i.Equals(COM))
                        ComBox.SelectedIndex = count;
                    count++;
                }
                count = 0;
                foreach (var i in RateBox.Items)
                {
                    if (i.Equals(Baudrate))
                        RateBox.SelectedIndex = count;
                    count++;
                }
            }
            catch { }
        }

        private void CreateConfig()
        {
            try
            {
                string path = string.Format($"{AppDomain.CurrentDomain.BaseDirectory}Config.txt");
                using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
                {
                    sw.WriteLine(COM);
                    sw.WriteLine(Baudrate);
                }
            }
            catch { }
        }
        #endregion

        #region Read, Write
        private void SHS_OnReadByte(object sender, ByteEventArgs e)
        {
            try
            {
                Dispatcher.Invoke(() => { ControlConnection.ShowRead(); });
                string a = "";
                foreach (var i in e.Byte)
                {
                    a += i.ToString("X2");
                    a += " ";
                }
                Dispatcher.Invoke(() => { Display.AddTextToLog(a, Brushes.Green); });
            }
            catch
            {

            }
        }

        private void SHS_OnWriteByte(object sender, ByteEventArgs e)
        {
            try
            {
                ControlConnection.ShowWrite();
                string a = "";
                foreach (var i in e.Byte)
                {
                    a += i.ToString("X2");
                    a += " ";
                }
                Display.AddTextToLog(a, Brushes.Red);
            }
            catch { }
        }
        #endregion

        #region Private GrozaS
        private bool CommandSetS = false;
        private bool[] S = new bool[] { false, false, false };
        private bool CommandResetS = false;
        #endregion

        #region Private GrozaZ1
        private bool CommandSet = false;
        private bool[] Z = new bool[] { false, false, false, false, false};
        private bool CommandReset = false;
        #endregion

        #region CommandForCrozaZ1
        private void GrozaZ1_OnSetParam(object sender, SetParamFWSGNSSEventArgs e)
        {
            try
            {
                var paramFWS = new TParamFWS[e.Param.Tables.Count];
                for (int i = 0; i < e.Param.Tables.Count; i++)
                {
                    if (e.Param.Tables[i].Hindrance == TableFreq.hid.FirstParam)
                    {
                        paramFWS[i] = new TParamFWS() { Freq = (uint)e.Param.Tables[i].FreqKHz, Deviation= 0, Manipulation = 0, Modulation = 0 };
                    }
                    else if (e.Param.Tables[i].Hindrance == TableFreq.hid.SecondParam)
                    {
                        paramFWS[i] = new TParamFWS() { Freq = (uint)e.Param.Tables[i].FreqKHz, Deviation = 0, Manipulation = (byte)e.Param.Tables[i].Man, Modulation = 1 };
                    }
                    else if (e.Param.Tables[i].Hindrance == TableFreq.hid.ThirdParam)
                    {
                        paramFWS[i] = new TParamFWS() { Freq = (uint)e.Param.Tables[i].FreqKHz, Deviation = e.Param.Tables[i].Dev, Manipulation = (byte)e.Param.Tables[i].Man, Modulation = 4 };
                    }
                }
                foreach (var i in paramFWS)
                {
                    if (i.Freq >= 350000 && i.Freq <= 500000)
                    {
                        Z[0] = true;
                    }
                    else if (i.Freq >= 800000 && i.Freq <= 1500000)
                    {
                        Z[1] = true;
                    }
                    else if (i.Freq >= 2000000 && i.Freq <= 2700000)
                    {
                        Z[2] = true;
                    }
                    else if (i.Freq >= 5600000 && i.Freq <= 5900000)
                    {
                        Z[3] = true;
                    }
                }
                Z[4] = true;
                CommandSet = true;
                var gnss = new TGnss() { gpsL1 = e.NaVi.GPS_L1, gpsL2 = e.NaVi.GPS_L2, glnssL1 = e.NaVi.Glonass_L1, glnssL2 = e.NaVi.Glonass_L2 };
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Установить параметры РП для ИРИ ФРЧ и системы навигации. Включить излучение заданной длительности", Brushes.Red);
                sHS.SendParamFwsGnss(Convert.ToInt32(TimeBox.Value), gnss,  paramFWS);
            }
            catch { }
        }

        private void GrozaZ1_OnSetPres(object sender, SetPresEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Настройка преселектора", Brushes.Red);
                sHS.SendParamPreselector(e.Pres);
            }
            catch { }
        }

        private void GrozaZ1_OnOffRadAndSetPres(object sender, LetterEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Выключить излучение и Включить преселектор", Brushes.Red);
                sHS.SendPreselectorOn(e.Letter);
                CommandReset = true;
            }
            catch { }
        }
        #endregion
        
        #region Full Status
        public static event EventHandler<ConfirmFullStatusEventArgs> OnAddFullStatusToSecondTable;

        private void SHS_OnConfirmFullStatus(object sender, ConfirmFullStatusEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                {
                    Dispatcher.Invoke(() => { Display.AddTextToLog($"Команда: {e.Amp}", Brushes.Green); });
                    Dispatcher.Invoke(() => { Display.AddTextToLog($"Код ошибки: {e.CodeError}", Brushes.Green); });
                }
                string a = e.Relay ? "ON" : "OFF";
                Dispatcher.Invoke(() => { RelayLabel.Content = a; });

                if(CommandSet/* && e.CodeError == 0*/)
                {
                    for (int i = 0; i < e.ParamAmp.Length; i++)
                    {
                        if(/*e.ParamAmp[i].Error == 0 && */Z[i])
                        {
                            GrozaZ1.ZLit[i] = true;
                        }
                    }
                    CommandSet = false;
                }

                if (CommandReset/* && e.CodeError == 0*/)
                {
                    for (int i = 0; i < e.ParamAmp.Length; i++)
                    {
                        if (/*e.ParamAmp[i].Error == 0 &&*/Z[i])
                        {
                            GrozaZ1.ZLit[i] = false;
                            Z[i] = false;
                        }
                    }
                    CommandReset = false;
                }
                OnAddFullStatusToSecondTable?.Invoke(this, e);
            }
            catch
            { }
        }

        private void GrozaZ1_OnFullStatus(object sender, LetterEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Полный статус", Brushes.Red);
                sHS.SendFullStatus(e.Letter);
            }
            catch { }
        }
        #endregion

        #region CommandForGrozaS
        #region RelaySwitch
        private void GrozaS_OnRelaySwitching(object sender, LetterEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Переключение реле", Brushes.Red);
                sHS.SendRelaySwitching(e.Letter);
            }
            catch { }
        }
        #endregion

        private void GrozaS_OnParam(object sender, ParamEventArgs e)
        {
            try
            {
                var paramFWS = new TParamFWS[e.Tables.Count];
                for (int i = 0; i < e.Tables.Count; i++)
                {
                    if (e.Tables[i].Hindrance == TableFreq.hid.FirstParam)
                    {
                        paramFWS[i] = new TParamFWS() { Freq = (uint)e.Tables[i].FreqKHz, Deviation = 0, Manipulation = 0, Modulation = 0 };
                    }
                    else if (e.Tables[i].Hindrance == TableFreq.hid.SecondParam)
                    {
                        paramFWS[i] = new TParamFWS() { Freq = (uint)e.Tables[i].FreqKHz, Deviation = 0, Manipulation = (byte)e.Tables[i].Man, Modulation = 1 };
                    }
                    else if (e.Tables[i].Hindrance == TableFreq.hid.ThirdParam)
                    {
                        paramFWS[i] = new TParamFWS() { Freq = (uint)e.Tables[i].FreqKHz, Deviation = e.Tables[i].Dev, Manipulation = (byte)e.Tables[i].Man, Modulation = 4 };
                    }
                }

                foreach (var i in paramFWS)
                {
                    if (i.Freq >= 100000 && i.Freq < 500000)
                    {
                        S[0] = true;
                    }
                    else if (i.Freq >= 500000 && i.Freq < 2500000)
                    {
                        S[1] = true;
                    }
                    else if (i.Freq >= 2500000 && i.Freq < 6000000)
                    {
                        S[2] = true;
                    }
                }
                CommandSetS = true;

                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Установить параметры РП для ИРИ ФРЧ и включить излучение заданной длительности", Brushes.Red);
                if (e.PowerBool == false)
                {
                    sHS.SendSetParamFWS(Convert.ToInt32(TimeBox.Value), paramFWS);
                }
                else
                {
                    sHS.SendSetParamFWSB(Convert.ToInt32(TimeBox.Value), paramFWS, e.PowerByte);
                }
            }
            catch { }
        }

        #region Reset
        private void GrozaS_OnReset(object sender, LetterEventArgs e)
        {
            try
            {
                CommandResetS = true;
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Сброс", Brushes.Red);
                sHS.SendReset(e.Letter);
            }
            catch { }
        }
        #endregion

        #region Off radiat
        private void GrozaS_OnOffRad(object sender, LetterEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Выключить излучение", Brushes.Red);
                sHS.SendRadiatOff(e.Letter);
            }
            catch { }
        }
        #endregion

        #region Spoof
        private void GrozaS_OnSpoof(object sender, SetSpoofEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Включить спуфинг", Brushes.Red);
                if (e.PowerBool == false)
                {
                    sHS.SendSetSPOOF();
                }
                else
                {
                    sHS.SendSetSPOOFB(e.PowerByte);
                }
            }
            catch { }
        }
        #endregion

        #region SetGnss
        private void SHS_OnConfirmSet(object sender, ConfirmSetEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                {
                    Dispatcher.Invoke(() => { Display.AddTextToLog($"Команда: {e.AmpCode}", Brushes.Green); });
                    Dispatcher.Invoke(() => { Display.AddTextToLog($"Код ошибки: {e.CodeError}", Brushes.Green); });
                }
            }
            catch { }
        }

        private void GrozaS_OnSetNaVi(object sender, SetNaViEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Установка подавляемых систем навигации и включение излучения", Brushes.Red);
                if (e.PowerBool == false)
                {
                    sHS.SendSetGNSS(new TGnss() { gpsL1 = e.GPS_L1, gpsL2 = e.GPS_L2, glnssL1 = e.Glonass_L1, glnssL2 = e.Glonass_L2 });
                }
                else
                {
                    sHS.SendSetGNSSB(new TGnss() { gpsL1 = e.GPS_L1, gpsL2 = e.GPS_L2, glnssL1 = e.Glonass_L1, glnssL2 = e.Glonass_L2 }, e.PowerByte);
                }
            }
            catch { }
        }

        
        #endregion
        
        #region Status
        public static event EventHandler<ConfirmStatusEventArgs> OnAddStatusToSecondTable;
        private void SHS_OnConfirmStatus(object sender, ConfirmStatusEventArgs e)
        {
            try
            {
                if (CommandSet/* && e.CodeError == 0*/)
                {
                    for (int i = 0; i < e.ParamAmp.Length; i++)
                    {
                        if (/*e.ParamAmp[i].Error == 0 && */Z[i])
                        {
                            GrozaS.SLit[i] = true;
                        }
                    }
                    CommandSetS = false;
                }

                if (CommandReset/* && e.CodeError == 0*/)
                {
                    for (int i = 0; i < e.ParamAmp.Length; i++)
                    {
                        if (/*e.ParamAmp[i].Error == 0 && */Z[i])
                        {
                            GrozaS.SLit[i] = false;
                            S[i] = false;
                        }
                    }
                    CommandResetS = false;
                }
                OnAddStatusToSecondTable?.Invoke(this, e);
            }
            catch { }
        }

        private void GrozaS_OnStatus(object sender, LetterEventArgs e)
        {
            try
            {
                if (Display.ShowAllByte == false)
                    Display.AddTextToLog("Статус", Brushes.Red);
                sHS.SendStatus(e.Letter);
            }
            catch { }
        }
        #endregion
        #endregion

        private void SHS_OnDisconnect(object sender, bool e)
        {
            try
            {
                if (e)
                {
                    connectFlag = false;
                    Dispatcher.Invoke(() => { ControlConnection.ShowDisconnect(); });
                }
            }
            catch { }
        }

        public ComSHS sHS = new ComSHS(0x04, 0x05);
        
        private void IntitComPort()
        {
            RelayLabel.Content = "Null";
            int[] k = new int[201];

            for(int i = 0; i<201; i++)
            {
                k[i] = i;
            }

            //foreach(var a in k)
            //{
            //    TimeBox.Items.Add(a.ToString());
            //}
            //TimeBox.SelectedIndex = 0;

            TimeBox.Value = 0;
            TimeBox.Minimum = 0;
            TimeBox.Maximum = 200;

            foreach (string PortName in SerialPort.GetPortNames())
            {
                ComBox.Items.Add(PortName);
            }
            ComBox.SelectedIndex = 0;

            string[] listbaudrate = new string[15];
            listbaudrate[0] = "110";
            listbaudrate[1] = "300";
            listbaudrate[2] = "600";
            listbaudrate[3] = "1200";
            listbaudrate[4] = "2400";
            listbaudrate[5] = "4800";
            listbaudrate[6] = "9600";
            listbaudrate[7] = "14400";
            listbaudrate[8] = "19200";
            listbaudrate[9] = "38400";
            listbaudrate[10] = "56000";
            listbaudrate[11] = "57600";
            listbaudrate[12] = "115200";
            listbaudrate[13] = "128000";
            listbaudrate[14] = "256000";

            foreach (string baudRate in listbaudrate)
            {
                RateBox.Items.Add(baudRate);
            }
            RateBox.SelectedIndex = 6;
        }

        private void GrozaSButton_Click(object sender, RoutedEventArgs e)
        {
            GrozaSButton.Background = Brushes.Gray;
            GrozaZ1Button.Background = Brushes.White;
            GrozaZ1.Visibility = Visibility.Collapsed;
            GrozaS.Visibility = Visibility.Visible;
        }

        private void GrozaZ1Button_Click(object sender, RoutedEventArgs e)
        {
            GrozaZ1Button.Background = Brushes.Gray;
            GrozaSButton.Background = Brushes.White;
            GrozaS.Visibility = Visibility.Collapsed;
            GrozaZ1.Visibility = Visibility.Visible;
        }
        
        private bool connectFlag = true;

        private void ControlConnection_ButServerClick(object sender, RoutedEventArgs e)
        {
            try
            {
                if (connectFlag)
                {
                    connectFlag = sHS.OpenPort(ComBox.SelectedItem.ToString(), Convert.ToInt32(RateBox.SelectedItem), Parity.None, 8, StopBits.One);
                    if (connectFlag)
                    {
                        ControlConnection.ShowConnect();
                        ControlConnection.ShowRead();
                        ControlConnection.ShowWrite();
                        COM = ComBox.SelectedItem.ToString();
                        Baudrate = RateBox.SelectedItem.ToString();
                        CreateConfig();
                    }
                    connectFlag = false;
                }
                else
                {
                    connectFlag = sHS.ClosePort();
                    ControlConnection.ShowDisconnect();
                    connectFlag = true;
                }
            }
            catch { }
        }
    }
}
