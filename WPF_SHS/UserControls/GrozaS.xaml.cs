﻿using SHS_DLL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace WPF_SHS
{
    /// <summary>
    /// Логика взаимодействия для GrozaS.xaml
    /// </summary>
    public partial class GrozaS : UserControl
    {
        public GrozaS()
        {
            InitializeComponent();
            MainWindow.OnAddStatusToSecondTable += MainWindow_OnAddStatusToSecondTable;
            AddEmptyRowsToSecondTable();
            FreqTable.IsReadOnly = true;
            SFreqTable.IsReadOnly = true;
            NumericPower.Minimum = 0;
            NumericPower.Maximum = 15;
            NumericPower.Value = 0;
            SpoofNumeric.Minimum = 0;
            SpoofNumeric.Maximum = 15;
            SpoofNumeric.Value = 0;
            ReadConfig();
        }

        public bool[] SLit = new bool[] { false, false, false };
        private string[] RangeLit = new string[] { "100-500", "500-2500", "2500-6000", "GNSS", "SPOOF" };

        private void AddEmptyRowsToSecondTable()
        {
            for (int i = 0; i < 5; i++)
            {
                SFreqTable.Items.Add(new STableFreq() { Lite = (i+1).ToString(), LitDescrip = RangeLit[i], Degree = string.Empty, Amperage = string.Empty, Emitting = false, Error = true, Power = false, Signal = false });
            }
        }
         
        private void MainWindow_OnAddStatusToSecondTable(object sender, ConfirmStatusEventArgs e)
        {
            try
            {
                int count = 1;
                Dispatcher.Invoke(() => { SFreqTable.Items.Clear(); });
                CheckGrozaSToImgSourceConverter.i = 0;
                foreach (var i in e.ParamAmp)
                {
                    //if (count == 1)
                    //{
                    //    OnCheckLit(SLit[0], 0);
                    //}

                    //if (count == 2)
                    //{
                    //    OnCheckLit(SLit[1], 1);
                    //}

                    //if (count == 3)
                    //{
                    //    OnCheckLit(SLit[2], 2);
                    //}
                    

                    //if (count == 1 || count == 2 || count == 3)
                    //{
                        Dispatcher.Invoke(() => { SFreqTable.Items.Add(new STableFreq() { Lite = count.ToString(), LitDescrip = RangeLit[count-1], Signal = Convert.ToBoolean(i.Snt), Emitting = Convert.ToBoolean(i.Rad), Degree = i.Temp.ToString(), Amperage = i.Current.ToString(), Power = Convert.ToBoolean(i.Power), Error = Convert.ToBoolean(i.Error) }); });
                    //}
                    count++;
                }
            }
            catch { }
        }
        
        private void OnCheckLit(bool flag, int lit)
        {
            CheckGrozaSToImgSourceConverter.flag[lit] = flag == true ? true : false;
        }

        public event EventHandler<LetterEventArgs> OnStatus;
        public event EventHandler<SetNaViEventArgs> OnSetNaVi;
        public event EventHandler<SetSpoofEventArgs> OnSpoof;
        public event EventHandler<LetterEventArgs> OnOffRad;
        public event EventHandler<LetterEventArgs> OnReset;
        public event EventHandler<LetterEventArgs> OnRelaySwitching;
        public event EventHandler<ParamEventArgs> OnParam;

        private void NaViBox_Checked(object sender, RoutedEventArgs e)
        {
            GPS_L1.IsChecked = true;
            GPS_L2.IsChecked = true;
            Glonass_L1.IsChecked = true;
            Glonass_L2.IsChecked = true;
        }

        private void NaViBox_Unchecked(object sender, RoutedEventArgs e)
        {
            GPS_L1.IsChecked = false;
            GPS_L2.IsChecked = false;
            Glonass_L1.IsChecked = false;
            Glonass_L2.IsChecked = false;
        }

        private void StatusButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                OnStatus?.Invoke(this, new LetterEventArgs(0));
            }
            catch { }
        }

        private void SetNaViButton_Click(object sender, RoutedEventArgs e)
        {
            OnSetNaVi?.Invoke(this, new SetNaViEventArgs((bool)GPS_L1.IsChecked, (bool)GPS_L2.IsChecked, (bool)Glonass_L1.IsChecked, (bool)Glonass_L2.IsChecked, (bool)PowerButton.IsChecked, (byte)NumericPower.Value));
        }

        private void ParamButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                byte[] bytes = new byte[5];
                int x = FreqTable.SelectedIndex;
                List<TableFreq> tables = new List<TableFreq>();
                for (int i = 0; i < FreqTable.Items.Count; i++)
                {
                    FreqTable.SelectedIndex = i;
                    TableFreq item = FreqTable.SelectedItem as TableFreq;
                    tables.Add(item);
                    bytes[i] = item.Power;
                }
                FreqTable.SelectedIndex = x;
                OnParam?.Invoke(this, new ParamEventArgs(tables, (bool)PowerButton.IsChecked, bytes));
            }
            catch
            {
                MessageBox.Show("Выберите значение!");
            }
        }

        private void SpuffingButton_Click(object sender, RoutedEventArgs e)
        {
            OnSpoof?.Invoke(this, new SetSpoofEventArgs((bool)PowerButton.IsChecked, (byte)SpoofNumeric.Value));
        }

        private void OffRadButton_Click(object sender, RoutedEventArgs e)
        {
            OnOffRad?.Invoke(this, new LetterEventArgs(0));
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            OnReset?.Invoke(this, new LetterEventArgs(0));
        }

        private void SwapReleButton_Click(object sender, RoutedEventArgs e)
        {
            byte letter = new byte();
            if((bool)ReleToogle.IsChecked)
            {
                letter = 0;
            }
            else
            {
                letter = 1;
            }
            OnRelaySwitching?.Invoke(this, new LetterEventArgs(letter));
        }

        #region FreqTable
        private int myId = 1;
        
        private void WindowChange_onChange(object sender, TableFreq e)
        {
            try
            {
                int ind = 0;
                if (FreqTable.SelectedIndex > -1)
                {
                    int L = 0;
                    string Descrip = string.Empty;
                    ind = FreqTable.SelectedIndex;
                    var item = (TableFreq)FreqTable.SelectedItem;
                    if ((e.FreqKHz >= 100000 && e.FreqKHz < 500000) || (e.FreqKHz >= 500000 && e.FreqKHz < 2500000) || (e.FreqKHz >= 2500000 && e.FreqKHz < 6000000))
                    {
                        FreqTable.Items.RemoveAt(ind);
                        if (e.FreqKHz >= 100000 && e.FreqKHz < 500000)
                        {
                            L = 1;
                            Descrip = "100-500";
                        }
                        else if (e.FreqKHz >= 500000 && e.FreqKHz < 2500000)
                        {
                            L = 2;
                            Descrip = "500-2500";
                        }
                        else if (e.FreqKHz >= 2500000 && e.FreqKHz < 6000000)
                        {
                            L = 3;
                            Descrip = "2500-6000";
                        }
                        if (L != 0 && CheckLit(L) == 0)
                        {
                            if (e.Hindrance == TableFreq.hid.FirstParam)
                            {
                                FreqTable.Items.Insert(ind, new TableFreq() { Id = ind + 1, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = L, Dev = e.Dev, Man = e.Man, ForDataGrid = "Без модуляции", Power = e.Power });
                            }
                            else if (e.Hindrance == TableFreq.hid.SecondParam)
                            {
                                FreqTable.Items.Insert(ind, new TableFreq() { Id = ind + 1, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = L, Dev = e.Dev, Man = e.Man, ForDataGrid = $"КФМ {e.Man} мкс", Power = e.Power });
                            }
                            else
                            {
                                FreqTable.Items.Insert(ind, new TableFreq() { Id = ind + 1, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = L, Dev = e.Dev, Man = e.Man, ForDataGrid = $"ЛЧМ {e.Dev} кГц {e.Man} кГц/c" , Power = e.Power });
                            }
                            FreqTable.SelectedIndex = ind;
                            CreateConfig();
                            windowChange.Close();
                        }
                        else
                        {
                            FreqTable.Items.Insert(ind, item);
                            FreqTable.SelectedIndex = ind;
                            MessageBox.Show("Значение для литеры существует!");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Проверьте вводимые данные!");
                    }
                }
            }
            catch { }
        }

        AddWindowS windowAdd;

        public static event EventHandler<TableFreq> Change;

        private void BAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                windowAdd = new AddWindowS();
                windowAdd.OnAdd += Window_onAdd;
                windowAdd.ShowDialog();
            }
            catch
            {
                windowAdd.Close();
            }
        }

        private int CheckLit(int e)
        {
            int count = 0;
            for (int i = 0; i < FreqTable.Items.Count; i++)
            {
                FreqTable.SelectedIndex = i;
                TableFreq item = FreqTable.SelectedItem as TableFreq;
                if (e == item.Lit)
                    count++;
            }
            return count;
        }

        private void Window_onAdd(object sender, TableFreq e)
        {
            int L = 0;
            string Descrip = string.Empty;
            if ((e.FreqKHz >= 100000 && e.FreqKHz < 500000) || (e.FreqKHz >= 500000 && e.FreqKHz < 2500000) || (e.FreqKHz >= 2500000 && e.FreqKHz < 6000000))
            {

                if (e.FreqKHz >= 100000 && e.FreqKHz < 500000)
                {
                    L = 1;
                    Descrip = "100-500";
                }
                else if (e.FreqKHz >= 500000 && e.FreqKHz < 2500000)
                {
                    L = 2;
                    Descrip = "500-2500";
                }
                else if (e.FreqKHz >= 2500000 && e.FreqKHz < 6000000)
                {
                    L = 3;
                    Descrip = "2500-6000";
                }
                if (L!= 0 && CheckLit(L) == 0)
                {
                    if (e.Hindrance == TableFreq.hid.FirstParam)
                    {
                        FreqTable.Items.Add(new TableFreq() { Id = myId, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = L, Dev = e.Dev, Man = e.Man, ForDataGrid = "Без модуляции", Power = e.Power });
                    }
                    else if (e.Hindrance == TableFreq.hid.SecondParam)
                    {
                        FreqTable.Items.Add(new TableFreq() { Id = myId, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = L, Dev = e.Dev, Man = e.Man, ForDataGrid = $"КФМ {e.Man} мкс", Power = e.Power });
                    }
                    else
                    {
                        FreqTable.Items.Add(new TableFreq() { Id = myId, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = L, Dev = e.Dev, Man = e.Man, ForDataGrid = $"ЛЧМ {e.Dev} кГц {e.Man} кГц/c", Power = e.Power });
                    }
                    FreqTable.SelectedIndex = FreqTable.Items.Count - 1;
                    myId++;
                    CreateConfig();
                    windowAdd.Close();
                }
                else
                {
                    MessageBox.Show("Значение для литеры существует!");
                }
            }
            else
            {
                MessageBox.Show("Проверьте вводимые данные!");
            }
        }

        private void BDelete_Click(object sender, RoutedEventArgs e)
        {
            if (FreqTable.SelectedIndex > -1)
            {
                FreqTable.Items.RemoveAt(FreqTable.SelectedIndex);
                for(int i = 0; i < FreqTable.Items.Count; i++)
                {
                    FreqTable.SelectedIndex = i;
                    TableFreq item = FreqTable.SelectedItem as TableFreq;
                    item.Id = i+1;
                }
                myId--;
                CreateConfig();
            }
        }

        private void BClear_Click(object sender, RoutedEventArgs e)
        {
            FreqTable.Items.Clear();
            myId = 1;
            CreateConfig();
        }

        ChangeWindowS windowChange;

        private void BChange_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (FreqTable.SelectedIndex > -1)
                {
                    windowChange = new ChangeWindowS();
                    windowChange.onChange += WindowChange_onChange;
                    var a = (TableFreq)FreqTable.SelectedItem;
                    Change?.Invoke(this, (TableFreq)FreqTable.SelectedItem);
                    windowChange.ShowDialog();
                }
            }
            catch
            {
                windowChange.Close();
            }
        }
        #endregion

        #region STable

        private void ReadConfig()
        {
            try
            {
                string[] s = File.ReadAllLines($"{AppDomain.CurrentDomain.BaseDirectory}STable.txt");

                for (int i = 0; i < s.Length; i+=8)
                {
                    if (Convert.ToByte(s[i+3]) == 0)
                    {
                        FreqTable.Items.Add(new TableFreq() { Id = Convert.ToInt32(s[i]), FreqKHz = Convert.ToInt32(s[i+1]), DFreqKHz = Convert.ToInt32(s[i+2]), Hindrance = TableFreq.hid.FirstParam, Lit = Convert.ToInt32(s[i+7]), Power = Convert.ToByte(s[i+6]), Dev = Convert.ToInt32(s[i+4]), Man = Convert.ToInt32(s[i + 5]), ForDataGrid = "Без модуляции" });
                    }
                    else if (Convert.ToByte(s[i + 3]) == 1)
                    {
                        FreqTable.Items.Add(new TableFreq() { Id = Convert.ToInt32(s[i]), FreqKHz = Convert.ToInt32(s[i + 1]), DFreqKHz = Convert.ToInt32(s[i + 2]), Hindrance = TableFreq.hid.SecondParam, Lit = Convert.ToInt32(s[i + 7]), Power = Convert.ToByte(s[i + 6]), Dev = Convert.ToInt32(s[i + 4]), Man = Convert.ToInt32(s[i + 5]), ForDataGrid = $"КФМ {Convert.ToInt32(s[i + 5])} мкс" });
                    }
                    else
                    {
                        FreqTable.Items.Add(new TableFreq() { Id = Convert.ToInt32(s[i]), FreqKHz = Convert.ToInt32(s[i + 1]), DFreqKHz = Convert.ToInt32(s[i + 2]), Hindrance = TableFreq.hid.ThirdParam, Lit = Convert.ToInt32(s[i + 7]), Power = Convert.ToByte(s[i + 6]), Dev = Convert.ToInt32(s[i + 4]), Man = Convert.ToInt32(s[i + 5]), ForDataGrid = $"ЛЧМ {Convert.ToInt32(s[i + 4])} кГц {Convert.ToInt32(s[i + 5])} кГц/c" });
                    }
                    myId++;
                }
            }
            catch { }
        }

        private void CreateConfig()
        {
            try
            {
                string path = string.Format($"{AppDomain.CurrentDomain.BaseDirectory}STable.txt");
                using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
                {
                    for (int i = 0; i < FreqTable.Items.Count; i++)
                    {
                        FreqTable.SelectedIndex = i;
                        TableFreq item = FreqTable.SelectedItem as TableFreq;
                        sw.WriteLine(item.Id);
                        sw.WriteLine(item.FreqKHz);
                        sw.WriteLine(item.DFreqKHz);
                        sw.WriteLine((byte)item.Hindrance);
                        sw.WriteLine(item.Dev);
                        sw.WriteLine(item.Man);
                        sw.WriteLine(item.Power);
                        sw.WriteLine(item.Lit);
                    }
                }
            }
            catch { }
        }

        #endregion
    }
}
