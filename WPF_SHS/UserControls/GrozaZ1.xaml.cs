﻿using SHS_DLL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Controls;

namespace WPF_SHS
{
    /// <summary>
    /// Логика взаимодействия для GrozaZ1.xaml
    /// </summary>
    public partial class GrozaZ1 : UserControl
    {
        public GrozaZ1()
        {
            InitializeComponent();
            FreqTable.IsReadOnly = true;
            SFreqTable.IsReadOnly = true;
            AddEmptyRowsToSecondTable();
            MainWindow.OnAddFullStatusToSecondTable += MainWindow_OnAddFullStatusToSecondTable;

            ReadConfig();
        }

        public bool[] ZLit = new bool[] { false, false, false, false, false };
        private string[] RangeLit = new string[] { "350-500", "800-1500", "2000-2700", "5600-5900", "GNSS" };

        private void AddEmptyRowsToSecondTable()
        {
            for (int i = 0; i < 5; i++)
            {
                SFreqTable.Items.Add(new STableFreq() { Lite = (i+1).ToString(), LitDescrip = RangeLit[i], Degree = string.Empty, Amperage = string.Empty, Emitting = false, Error = true, Power = false, Signal = false });
            }
        }

        private void MainWindow_OnAddFullStatusToSecondTable(object sender, ConfirmFullStatusEventArgs e)
        {
            try
            {
                int count = 1;
                Dispatcher.Invoke(() => { SFreqTable.Items.Clear(); });
                CheckGrozaZ1ToImgSourceConverter.i = 0;
                foreach (var i in e.ParamAmp)
                {
                    //if (count == 1)
                    //{
                    //    OnCheckLit(ZLit[0], 0);
                    //}

                    //if (count == 2)
                    //{
                    //    OnCheckLit(ZLit[1], 1);
                    //}

                    //if (count == 3)
                    //{
                    //    OnCheckLit(ZLit[2], 2);
                    //}

                    //if (count == 4)
                    //{
                    //    OnCheckLit(ZLit[3], 3);
                    //}

                    //if (count == 5)
                    //{
                    //    OnCheckLit(ZLit[4], 4);
                    //}

                    //if (count == 1 || count == 2 || count == 3 || count == 4 || count == 5)
                    //{
                        Dispatcher.Invoke(() => { SFreqTable.Items.Add(new STableFreq() { Lite = count.ToString(), LitDescrip = RangeLit[count-1], Signal = Convert.ToBoolean(i.Snt), Emitting = Convert.ToBoolean(i.Rad), Degree = i.Temp.ToString(), Amperage = i.Current.ToString(), Power = Convert.ToBoolean(i.Power), Error = Convert.ToBoolean(i.Error) }); });
                    //}
                    count++;
                }
            }
            catch
            {
            }
        }

        private void OnCheckLit(bool flag, int lit)
        {
            CheckGrozaZ1ToImgSourceConverter.flag[lit] = flag == true ? true : false;
        }

        public event EventHandler<LetterEventArgs> OnFullStatus;
        public event EventHandler<LetterEventArgs> OnOffRadAndSetPres;
        public event EventHandler<SetPresEventArgs> OnSetPres;
        public event EventHandler<SetParamFWSGNSSEventArgs> OnSetParam;

        private void CheckBox_Checked(object sender, RoutedEventArgs e)
        {
            firstCheck.IsChecked = true;
            secondCheck.IsChecked = true;
            thirdCheck.IsChecked = true;
            fourthCheck.IsChecked = true;
            fifthCheck.IsChecked = true;
            sixthCheck.IsChecked = true;
            seventhCheck.IsChecked = true;
            eighthCheck.IsChecked = true;
            ninthCheck.IsChecked = true;
            tenthCheck.IsChecked = true;
        }

        private void Preselector_Unchecked(object sender, RoutedEventArgs e)
        {
            firstCheck.IsChecked = false;
            secondCheck.IsChecked = false;
            thirdCheck.IsChecked = false;
            fourthCheck.IsChecked = false;
            fifthCheck.IsChecked = false;
            sixthCheck.IsChecked = false;
            seventhCheck.IsChecked = false;
            eighthCheck.IsChecked = false;
            ninthCheck.IsChecked = false;
            tenthCheck.IsChecked = false;
        }

        private void NaViBox_Checked(object sender, RoutedEventArgs e)
        {
            GPS_L1.IsChecked = true;
            GPS_L2.IsChecked = true;
            Glonass_L1.IsChecked = true;
            Glonass_L2.IsChecked = true;
        }

        private void NaViBox_Unchecked(object sender, RoutedEventArgs e)
        {
            GPS_L1.IsChecked = false;
            GPS_L2.IsChecked = false;
            Glonass_L1.IsChecked = false;
            Glonass_L2.IsChecked = false;
        }

        private void FullStatusButton_Click(object sender, RoutedEventArgs e)
        {
            OnFullStatus?.Invoke(this, new LetterEventArgs(0));
        }

        private void SetNaViButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int x = FreqTable.SelectedIndex;
                List<TableFreq> tables = new List<TableFreq>();
                for (int i = 0; i < FreqTable.Items.Count; i++)
                {
                    FreqTable.SelectedIndex = i;
                    TableFreq item = FreqTable.SelectedItem as TableFreq;
                    tables.Add(item);
                }
                FreqTable.SelectedIndex = x;
                OnSetParam?.Invoke(this, new SetParamFWSGNSSEventArgs(new SetNaViEventArgs((bool)GPS_L1.IsChecked, (bool)GPS_L2.IsChecked, (bool)Glonass_L1.IsChecked, (bool)Glonass_L2.IsChecked, false, 0), new ParamEventArgs(tables, false, new byte[0])));
                //TableFreq item = FreqTable.SelectedItem as TableFreq;

                //if (item.Hindrance == TableFreq.hid.FirstParam)
                //{
                //    OnSetParam?.Invoke(this, new SetParamFWSGNSSEventArgs(new SetNaViEventArgs((bool)GPS_L1.IsChecked, (bool)GPS_L2.IsChecked, (bool)Glonass_L1.IsChecked, (bool)Glonass_L2.IsChecked), new ParamEventArgs((uint)item.FreqKHz, (byte)item.Hindrance, 0, 0)));
                //}
                //else if (item.Hindrance == TableFreq.hid.SecondParam)
                //{
                //    OnSetParam?.Invoke(this, new SetParamFWSGNSSEventArgs(new SetNaViEventArgs((bool)GPS_L1.IsChecked, (bool)GPS_L2.IsChecked, (bool)Glonass_L1.IsChecked, (bool)Glonass_L2.IsChecked), new ParamEventArgs((uint)item.FreqKHz, (byte)item.Hindrance, 0, (byte)item.Man)));
                //}
                //else if (item.Hindrance == TableFreq.hid.ThirdParam)
                //{
                //    OnSetParam?.Invoke(this, new SetParamFWSGNSSEventArgs(new SetNaViEventArgs((bool)GPS_L1.IsChecked, (bool)GPS_L2.IsChecked, (bool)Glonass_L1.IsChecked, (bool)Glonass_L2.IsChecked), new ParamEventArgs((uint)item.FreqKHz, (byte)item.Hindrance, item.Dev, (byte)item.Man)));
                //}
            }
            catch
            {
                MessageBox.Show("Выберите значение!");
            }
        }

        private void OffRadAndOnPresButton_Click(object sender, RoutedEventArgs e)
        {
            OnOffRadAndSetPres?.Invoke(this, new LetterEventArgs(0));
        }

        private void SettingsPresButton_Click(object sender, RoutedEventArgs e)
        {
            bool[] presArray = new bool[]
            {
                (bool)firstCheck.IsChecked,
                (bool)secondCheck.IsChecked,
                (bool)thirdCheck.IsChecked,
                (bool)fourthCheck.IsChecked,
                (bool)fifthCheck.IsChecked,
                (bool)sixthCheck.IsChecked,
                (bool)seventhCheck.IsChecked,
                (bool)eighthCheck.IsChecked,
                (bool)ninthCheck.IsChecked,
                (bool)tenthCheck.IsChecked,
            };
            OnSetPres?.Invoke(this, new SetPresEventArgs(presArray));
        }

        #region FreqTable
        private int myId = 1;

        private int CheckLit(int e)
        {
            int count = 0;
            for (int i = 0; i < FreqTable.Items.Count; i++)
            {
                FreqTable.SelectedIndex = i;
                TableFreq item = FreqTable.SelectedItem as TableFreq;
                if (e == item.Lit)
                    count++;
            }
            return count;
        }
        
        private void WindowChange_onChange(object sender, TableFreq e)
        {
            int ind = 0;
            if (FreqTable.SelectedIndex > -1)
            {
                int L = 0;
                string Descrip = string.Empty;
                ind = FreqTable.SelectedIndex;
                var item = (TableFreq)FreqTable.SelectedItem;
                if ((e.FreqKHz >= 350000 && e.FreqKHz <= 500000) || (e.FreqKHz >= 800000 && e.FreqKHz <= 1500000) || (e.FreqKHz >= 2000000 && e.FreqKHz <= 2700000) || (e.FreqKHz >= 5600000 && e.FreqKHz <= 5900000))
                {
                    FreqTable.Items.RemoveAt(ind);
                    if (e.FreqKHz >= 350000 && e.FreqKHz <= 500000)
                    {
                        L = 1;
                        Descrip = "350-500";
                    }
                    else if (e.FreqKHz >= 800000 && e.FreqKHz <= 1500000)
                    {
                        L = 2;
                        Descrip = "800-1500";
                    }
                    else if (e.FreqKHz >= 2000000 && e.FreqKHz <= 2700000)
                    {
                        L = 3;
                        Descrip = "2000-2700";
                    }
                    else if (e.FreqKHz >= 5600000 && e.FreqKHz <= 5900000)
                    {
                        L = 4;
                        Descrip = "5600-5900";
                    }
                    if (L != 0 && CheckLit(L) == 0)
                    {
                        if (e.Hindrance == TableFreq.hid.FirstParam)
                        {
                            FreqTable.Items.Insert(ind, new TableFreq() { Id = ind + 1, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = L, Dev = e.Dev, Man = e.Man, ForDataGrid = "Без модуляции" });
                        }
                        else if (e.Hindrance == TableFreq.hid.SecondParam)
                        {
                            FreqTable.Items.Insert(ind, new TableFreq() { Id = ind + 1, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = L, Dev = e.Dev, Man = e.Man, ForDataGrid = $"КФМ {e.Man} мкс" });
                        }
                        else
                        {
                            FreqTable.Items.Insert(ind, new TableFreq() { Id = ind + 1, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = L, Dev = e.Dev, Man = e.Man, ForDataGrid = $"ЛЧМ {e.Dev} кГц {e.Man} кГц/c" });
                        }
                        FreqTable.SelectedIndex = ind;
                        CreateConfig();
                        windowChange.Close();
                    }
                    else
                    {
                        FreqTable.Items.Insert(ind, item);
                        FreqTable.SelectedIndex = ind;
                        MessageBox.Show("Значение для литеры существует!");
                    }
                }
                else
                {
                    MessageBox.Show("Проверьте вводимые данные!");
                }
            }
        }

        AddWindow windowAdd;
        public static event EventHandler<TableFreq> Change;

        private void BAdd_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                windowAdd = new AddWindow();
                windowAdd.OnAdd += Window_onAdd;
                windowAdd.ShowDialog();
            }
            catch
            {
                windowAdd.Close();
            }
        }

        private void Window_onAdd(object sender, TableFreq e)
        {
            int L = 0;
            string Descrip = string.Empty;
            if ((e.FreqKHz >= 350000 && e.FreqKHz <= 500000) || (e.FreqKHz >= 800000 && e.FreqKHz <= 1500000) || (e.FreqKHz >= 2000000 && e.FreqKHz <= 2700000) || (e.FreqKHz >= 5600000 && e.FreqKHz <= 5900000))
            {

                if (e.FreqKHz >= 350000 && e.FreqKHz <= 500000)
                {
                    L = 1;
                    Descrip = "350-500";
                }
                else if (e.FreqKHz >= 800000 && e.FreqKHz <= 1500000)
                {
                    L = 2;
                    Descrip = "800-1500";
                }
                else if (e.FreqKHz >= 2000000 && e.FreqKHz <= 2700000)
                {
                    L = 3;
                    Descrip = "2000-2700";
                }
                else if (e.FreqKHz >= 5600000 && e.FreqKHz <= 5900000)
                {
                    L = 4;
                    Descrip = "5600-5900";
                }
            }
            else
            {
                MessageBox.Show("Проверьте вводимые значения!");
                return;
            }
            if (CheckLit(L) == 0)
            {
                if (e.Hindrance == TableFreq.hid.FirstParam)
                {
                    FreqTable.Items.Add(new TableFreq() { Id = myId, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = L, Dev = 0, Man = 0, ForDataGrid = "Без модуляции" });
                }
                else if (e.Hindrance == TableFreq.hid.SecondParam)
                {
                    FreqTable.Items.Add(new TableFreq() { Id = myId, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = L, Dev = 0, Man = e.Man, ForDataGrid = $"КФМ {e.Man} мкс" });
                }
                else
                {
                    FreqTable.Items.Add(new TableFreq() { Id = myId, FreqKHz = e.FreqKHz, DFreqKHz = e.DFreqKHz, Hindrance = e.Hindrance, Lit = L, Dev = e.Dev, Man = e.Man, ForDataGrid = $"ЛЧМ {e.Dev} кГц {e.Man} кГц/c" });
                }
                myId++;
                CreateConfig();
                windowAdd.Close();
            }
            else
            {
                MessageBox.Show("Значение для литеры существует!");
            }
        }
        

        private void BDelete_Click(object sender, RoutedEventArgs e)
        {
            if (FreqTable.SelectedIndex > -1)
            {
                FreqTable.Items.RemoveAt(FreqTable.SelectedIndex);
                for (int i = 0; i < FreqTable.Items.Count; i++)
                {
                    FreqTable.SelectedIndex = i;
                    TableFreq item = FreqTable.SelectedItem as TableFreq;
                    item.Id = i+1;
                }
                myId--;
            }
        }

        private void BClear_Click(object sender, RoutedEventArgs e)
        {
            FreqTable.Items.Clear();
            myId = 1;
        }

        ChangeWindow windowChange;

        private void BChange_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (FreqTable.SelectedIndex > -1)
                {
                    windowChange = new ChangeWindow();
                    windowChange.onChange += WindowChange_onChange;
                    var a = (TableFreq)FreqTable.SelectedItem;
                    Change?.Invoke(this, (TableFreq)FreqTable.SelectedItem);
                    windowChange.ShowDialog();
                }
            }
            catch
            {
                windowChange.Close();
            }
        }
        #endregion

        #region ZTable

        private void ReadConfig()
        {
            try
            {
                string[] s = File.ReadAllLines($"{AppDomain.CurrentDomain.BaseDirectory}ZTable.txt");

                for (int i = 0; i < s.Length; i += 7)
                {
                    if (Convert.ToByte(s[i + 3]) == 0)
                    {
                        FreqTable.Items.Add(new TableFreq() { Id = Convert.ToInt32(s[i]), FreqKHz = Convert.ToInt32(s[i + 1]), DFreqKHz = Convert.ToInt32(s[i + 2]), Hindrance = TableFreq.hid.FirstParam, Lit = Convert.ToInt32(s[i + 6]), Dev = Convert.ToInt32(s[i + 4]), Man = Convert.ToInt32(s[i + 5]), ForDataGrid = "Без модуляции" });
                    }
                    else if (Convert.ToByte(s[i + 3]) == 1)
                    {
                        FreqTable.Items.Add(new TableFreq() { Id = Convert.ToInt32(s[i]), FreqKHz = Convert.ToInt32(s[i + 1]), DFreqKHz = Convert.ToInt32(s[i + 2]), Hindrance = TableFreq.hid.SecondParam, Lit = Convert.ToInt32(s[i + 6]), Dev = Convert.ToInt32(s[i + 4]), Man = Convert.ToInt32(s[i + 5]), ForDataGrid = $"КФМ {Convert.ToInt32(s[i + 5])} мкс" });
                    }
                    else
                    {
                        FreqTable.Items.Add(new TableFreq() { Id = Convert.ToInt32(s[i]), FreqKHz = Convert.ToInt32(s[i + 1]), DFreqKHz = Convert.ToInt32(s[i + 2]), Hindrance = TableFreq.hid.ThirdParam, Lit = Convert.ToInt32(s[i + 6]), Dev = Convert.ToInt32(s[i + 4]), Man = Convert.ToInt32(s[i + 5]), ForDataGrid = $"ЛЧМ {Convert.ToInt32(s[i + 4])} кГц {Convert.ToInt32(s[i + 5])} кГц/c" });
                    }
                    myId++;
                }
            }
            catch { }
        }

        private void CreateConfig()
        {
            try
            {
                string path = string.Format($"{AppDomain.CurrentDomain.BaseDirectory}ZTable.txt");
                using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
                {
                    for (int i = 0; i < FreqTable.Items.Count; i++)
                    {
                        FreqTable.SelectedIndex = i;
                        TableFreq item = FreqTable.SelectedItem as TableFreq;
                        sw.WriteLine(item.Id);
                        sw.WriteLine(item.FreqKHz);
                        sw.WriteLine(item.DFreqKHz);
                        sw.WriteLine((byte)item.Hindrance);
                        sw.WriteLine(item.Dev);
                        sw.WriteLine(item.Man);
                        sw.WriteLine(item.Lit);
                    }
                }
            }
            catch { }
        }

        #endregion
    }
}
